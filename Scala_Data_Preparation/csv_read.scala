import scala.collection.mutable.ArrayBuffer

object dataPreparation extends App {
  val rows = ArrayBuffer[Array[String]]()
  using(io.Source.fromFile("src/data.csv")) { source =>
    for (line <- source.getLines) {
      rows += line.split(";").map(_.trim)
    }
  }
  for (row <- rows) {
    println(s"${row(0)}=${row(1)}=${row(2)}=${row(3)}")
  }

  def using[A <: { def close(): Unit }, B](resource: A)(f: A => B): B =
    try {
      f(resource)
    } finally {
      resource.close()
    }
}
