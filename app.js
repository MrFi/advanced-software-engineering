"use strict";

/**
 * Created by fi on 11/22/17.
 */
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const routes = require('./routes/users');
const path = require('path');
const index = require('./routes/index');
const favicon = require('serve-favicon');

/**
 * Create App and define routes
 * @type {*}
 */
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/', index);
app.use('/crud', routes);
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));


/**
 * View engine setup
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


/**
 * MongoDB
 */
mongoose.connect('mongodb://localhost/bachelor');
const db = mongoose.connection;
db.on('error', function () {
    next("Error: Connection to mongodb failed");
});
db.once('open', function () {
    console.log('Connection to mongodb successful');
});


/**
 * Error Handler
 */
app.use(function errorHandler(err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }
    res.status(500);
    res.render('error', {error: err});
});

/**
 * Start server on localhost:3000
 */
app.listen(3000, function (err) {
    if (err !== undefined) {
        console.log('Error on startup', err);
    }
    else {
        console.log('Server is running on port 3000');
    }
});