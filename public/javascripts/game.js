const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");
const ballRadius = 10;
const paddleHeight = 10;
const paddleWidth = 75;
const brickRowCount = 5;
const brickColumnCount = 3;
const brickWidth = 75;
const brickHeight = 20;

/**
 * Variables for Game Calculation
 * @type {number}
 */
let x = canvas.width / 2;
let y = canvas.height - 30;
let dx = 2;
let dy = -2;
let paddleX = (canvas.width - paddleWidth) / 2;
let rightPressed = false;
let leftPressed = false;
let score = 0;
let lives = 3;
let requestId;

let bricks = [];
for (let c = 0; c < brickColumnCount; c++) {
    bricks[c] = [];
    for (let r = 0; r < brickRowCount; r++) {
        bricks[c][r] = {x: 0, y: 0, status: 1};
    }
}

/**
 * The Draw function with all other important functions to draw the game
 */
let draw = function () {
    let cancel = false;
    requestId = undefined;
    if (!requestId) {
        /**
         * Draw the ball
         */
        let drawBall = function () {
            ctx.beginPath();
            ctx.arc(x, y, ballRadius, 0, Math.PI * 2);
            ctx.fillStyle = "#c23616";
            ctx.fill();
            ctx.closePath();
        };
        /**
         * Draw the paddle as higher order function
         */
        let privatedrawPaddle = function () {
            return function () {
                ctx.beginPath();
                ctx.rect(paddleX, canvas.height - paddleHeight, paddleWidth, paddleHeight);
                ctx.fillStyle = "#2c3e50";
                ctx.fill();
                ctx.closePath();
            }
        };
        /**
         * Draw the Score as higher order function
         */
        let privateDrawScore = function () {
            return function () {
                ctx.font = "16px Arial";
                ctx.fillStyle = "#2c3e50";
                ctx.fillText("Score: " + score, 8, 20);
            }
        };
        /**
         * Draw the Lives as higher order function
         */
        let privateDrawLives = function () {
            return function () {
                ctx.font = "16px Arial";
                ctx.fillStyle = "#2c3e50";
                ctx.fillText("Lives: " + lives, canvas.width - 65, 20);
            }
        };
        /**
         * Save the returned function of the higher order function, so that we can call it
         */
        let drawLives = privateDrawLives();
        let drawScore = privateDrawScore();
        let drawPaddle = privatedrawPaddle();

        /**
         * Draw the bricks
         */
        let drawBricks = function () {
            const brickPadding = 10;
            const brickOffsetTop = 30;
            const brickOffsetLeft = 30;
            for (let c = 0; c < brickColumnCount; c++) {
                for (let r = 0; r < brickRowCount; r++) {
                    if (bricks[c][r].status === 1) {
                        let brickX = (r * (brickWidth + brickPadding)) + brickOffsetLeft;
                        let brickY = (c * (brickHeight + brickPadding)) + brickOffsetTop;
                        bricks[c][r].x = brickX;
                        bricks[c][r].y = brickY;
                        ctx.beginPath();
                        ctx.rect(brickX, brickY, brickWidth, brickHeight);
                        ctx.fillStyle = "#2c3e50";
                        ctx.fill();
                        ctx.closePath();
                    }
                }
            }
        };
        /**
         * Detect collisions and use swal() to inform the user that he is the winner
         */
        let collisionDetection = function () {
            for (let c = 0; c < brickColumnCount; c++) {
                for (let r = 0; r < brickRowCount; r++) {
                    let b = bricks[c][r];
                    if (b.status === 1) {
                        if (x > b.x && x < b.x + brickWidth && y > b.y && y < b.y + brickHeight) {
                            dy = -dy;
                            b.status = 0;
                            score++;
                            if (score === brickRowCount * brickColumnCount) {
                                cancel = true;
                                addUser(lives, score);
                                swal(
                                    'You won!',
                                    'Your Result was saved in the Ranklist.',
                                    'success'
                                ).then(() => {
                                    document.location.reload();
                                });
                            }
                        }
                    }
                }
            }
        };

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        drawBricks();
        drawBall();
        drawPaddle();
        drawScore();
        drawLives();
        collisionDetection();

        if (x + dx > canvas.width - ballRadius || x + dx < ballRadius) {
            dx = -dx;
        }
        if (y + dy < ballRadius) {
            dy = -dy;
        }
        else if (y + dy > canvas.height - ballRadius) {
            if (x > paddleX && x < paddleX + paddleWidth) {
                dy = -dy;
            }
            else {
                lives--;
                if (!lives) {
                    cancel = true;
                    addUser(lives, score);
                    swal(
                        'GAME OVER!',
                        'Your Result was saved in the Ranklist.',
                        'error'
                    ).then(() => {
                        document.location.reload();
                    });
                }
                else {
                    x = canvas.width / 2;
                    y = canvas.height - 30;
                    dx = 3;
                    dy = -3;
                    paddleX = (canvas.width - paddleWidth) / 2;
                }
            }
        }

        if (rightPressed && paddleX < canvas.width - paddleWidth) {
            paddleX += 7;
        }
        else if (leftPressed && paddleX > 0) {
            paddleX -= 7;
        }

        x += dx;
        y += dy;
        /**
         * Request the draw again when cancel = false. Else stop requesting
         */
        if (cancel !== true) {
            requestId = window.requestAnimationFrame(draw);
        } else {
            stopRequest();
        }
    }
};
/**
 * Stop function to stopRequest the request:  window.requestAnimationFrame(draw)
 */
let stopRequest = function () {
    if (requestId) {
        window.cancelAnimationFrame(requestId);
        requestId = undefined;
    }
};

/**
 * Keycode Down Handler
 * @param e
 */
let keyDownHandler = function (e) {
    if (e.keyCode === 39) {
        rightPressed = true;
    }
    else if (e.keyCode === 37) {
        leftPressed = true;
    }
};

/**
 * Key Up Handler
 * @param e
 */
let keyUpHandler = function (e) {
    if (e.keyCode === 39) {
        rightPressed = false;
    }
    else if (e.keyCode === 37) {
        leftPressed = false;
    }
};

/**
 * Mouse Move Handler
 * @param e
 */
let mouseMoveHandler = function (e) {
    let relativeX = e.clientX - canvas.offsetLeft;
    if (relativeX > 0 && relativeX < canvas.width) {
        paddleX = relativeX - paddleWidth / 2;
    }
};
/**
 * Behaviour for use of arrow keys in the game
 */
document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);
document.addEventListener("mousemove", mouseMoveHandler, false);