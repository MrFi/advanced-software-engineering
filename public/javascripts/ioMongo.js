/**
 * Fill the table frm index.jade with content from Database with GET
 */
let populateTable = function () {
    let tableContent = '';
    $.getJSON('crud/', function (data) {
        $.each(data, function () {
            tableContent += '<tr>';
            tableContent += '<td class="text-left">' + this.name + '</td>';
            tableContent += '<td class="text-left">' + this.score + '</td>';
            tableContent += '<td class="text-left">' + this.lives + '</td>';
            tableContent += '</tr>';
        });
        $('#tableRank tbody').html(tableContent);
    });
};

/**
 * Save the Score and rest lives from user in Database with POST
 * @param lives
 * @param score
 */
let addUser = function (lives, score) {
    let newUser = {
        'name': 'The Boring Gamer',
        'score': score,
        'lives': lives
    };
    $.ajax({
        type: 'POST',
        data: newUser,
        url: 'crud/',
        dataType: 'JSON'
    })
};