/**
 * Set visibility and display of ui components
 * @param id
 * @param txt
 */
let setVisibility = function (id, txt) {
    document.getElementById(id).style.visibility = txt;
};

let setDisplay = function (id, txt) {
    document.getElementById(id).style.display = txt;
};

/**
 * Check the type of the Resource:
 * @param itemType
 */
let changeWindows = function (itemType) {
    switch (itemType) {
        case 'gameWindow':
            //Show gameWindow
            setVisibility('gameWindow', 'visible');
            setDisplay('gameWindow', 'block');

            //Hide startWindow
            setVisibility('startScreen', 'hidden');
            setDisplay('startScreen', 'none');
            break;

        case 'tableDB':
            //Show tableWindow
            setVisibility('tableDB', 'visible');
            setDisplay('tableDB', 'block');

            //Hide gameWindow
            setVisibility('gameWindow', 'hidden');
            setDisplay('gameWindow', 'none');

            //Hide startWindow
            setVisibility('startScreen', 'hidden');
            setDisplay('startScreen', 'none');
            break;

        case 'startScreen':
            //Show startWindow
            setVisibility('startScreen', 'visible');
            setDisplay('startScreen', 'block');

            //hide tableWindow
            setVisibility('tableDB', 'hidden');
            setDisplay('tableDB', 'hidden');

            //Hide gameWindow
            setVisibility('gameWindow', 'hidden');
            setDisplay('gameWindow', 'none');
            break;
    }
};



