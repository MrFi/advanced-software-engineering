**recommender-system**

Install mongodb
https://docs.mongodb.com/manual/installation/

**Install all relevant packages in project directory**

npm install


**Create mongodb Folder in root**


mkdir mongodb


**Run with**


npm run start


**Download the Documentation**
https://bitbucket.org/MrFi/advanced-software-engineering/src/154491e502a79b86179c7f17bc5cbe8f03be0658/pet_project_viktor_treu_879848.pdf?at=master&fileviewer=file-view-default

