let express = require('express');
let userModel = require('../schema/userSchema');
let mongoose = require('mongoose');
let router = express.Router();

/**
 * Render the index.jade file
 */
router.get('/', function (req, res, next) {
    res.render('index', {layout: 'layout', json: res.locals.items});
});

module.exports = router;