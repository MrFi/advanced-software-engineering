'use strict';

/**
 * Middleware to send the item as JSON
 * @param req
 * @param res
 * @param next
 */
function sendResponse(req, res, next) {
    if (res.locals.items) {
        res.json(res.locals.items);
        delete res.locals.items;
    } else {
        res.set('Content-Type', 'application/json');
        res.status(204).json({});
    }
}
/**
 * Error handler
 * @param req
 * @param res
 * @param next
 */
function notFound(req, res, next) {
    let err = new Error('Api route not found');
    err.status = 404;
    next(err);
}

module.exports = {
    sendResponse: sendResponse,
    notFound: notFound
};