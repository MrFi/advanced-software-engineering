let mongoose = require('mongoose');
let Schema = mongoose.Schema;

/**
 * Define database schema for saving new data
 */
let userSchema = new Schema({
    name: {type: String, default: ''},
    score: {type: String, default: ''},
    lives: {type: String, default: ''}
});

module.exports = mongoose.model('User', userSchema);